const pathMap = {
  "/index.html": document.getElementsByClassName('home'),
  '/about.html': document.getElementsByClassName('about'),
  '/contact.html': document.getElementsByClassName('contact'),
  '/projects.html': document.getElementsByClassName('projects'),
};
const path = location.pathname;

console.log(path)

if (pathMap[path]) {
  for (var key in pathMap){
    console.log(key)
    if(key != path){
      console.log(pathMap[key])
      for(let i = 0; i < pathMap[key].length; i++){
        pathMap[key][i].classList.remove('active-dark');
      }
    }
  }
  for(let i = 0; i < pathMap[path].length; i++){
    pathMap[path][i].classList.add('active-dark');
  }
}

